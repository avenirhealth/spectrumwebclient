# Rationale

Project developed to demonstrate use of standard HTTP client ([axois](https://github.com/mzabriskie/axios)) to access RemObjects JSON RPC API.

Key files are

- `./src/spectrumWebService.js`
- `./src/App.js`

The remainder of the application is [`create-react-app`](https://github.com/facebookincubator/create-react-app) boilerplate.
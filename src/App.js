import React, { Component } from 'react';
import './App.css';
import { logIn, logOut, createZimbabweProjection, calculateProjection, getIndicators } from './spectrumWebService';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        indicators: []
    };
  }

  componentWillMount() {
      logIn()
          .then(response => createZimbabweProjection(response.data.id))
          .then(response => calculateProjection(response.data.id, 1))
          .then(response => getIndicators(response.data.id, "GB_DP"))
          .then(response => {
              logOut(response.data.id);

              return response;
          })
          .then(response => this.setState({
            ...this.state,
            indicators: JSON.parse(response.data.result.Result).result,
            error: undefined
          })).catch(error => {
            console.log(error);
            // console.log(error.response.data.error);

            this.setState({
              ...this.state,
              error: error//.response.data.error.message
            })
          });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>Spectrum Web Service &quot;REST&quot; demo</h1>
        </div>
          {this.state.indicators && this.state.indicators.length > 0
            ?
              <ul>
                  {this.state.indicators.map((indicator, index) => (
                      <li key={index + 1}>{indicator.Name} [{indicator.IndId}]</li>
                  ))}
              </ul>
            :
              <p className="App-intro">There are no indicators loaded</p>
          }

          {this.state.error ? <p className="App-error">{`Spectrum Web Service error: ${this.state.error}`}</p> : null}
      </div>
    );
  }
}

export default App;

import axios from 'axios';

const jsonServiceUrl = "http://spectrumweb1.eastus2.cloudapp.azure.com:8099/JSON";

const makeRequest = (sessionId, method, params, message) =>
    axios.post(jsonServiceUrl, {
        id: sessionId,
        method: method,
        params: {
            ...params,
            jsonMesg: JSON.stringify(message)
        }
    });

export const getLogInStatus = sessionId =>
    makeRequest(sessionId, "GBLoginService.SignedIn", {}, {});

export const logIn = () =>
    makeRequest(undefined, "GBLoginService.Login", {
        username: "ewerst@avenirhealth.org",
        password: "password"
    }, {});

export const logOut = sessionId =>
    makeRequest(sessionId, "GBLoginService.LogOut", {}, {});

export const createProjection = (sessionId, random, countryIso, fileName, firstYear, finalYear, modList) =>
    makeRequest(sessionId, "GBSpectrumService.CreateProjection", {}, {
        Random: random,
        CountryISO: countryIso,
        FileName: fileName,
        FirstYear: firstYear,
        FinalYear: finalYear,
        ModList: modList
    });

export const createZimbabweProjection = sessionId =>
    createProjection(sessionId, 716, 716, "TestProjection1.PJNZ", 1970, 2020, [
        "DP", "AM", "CS"
    ]);

export const getProjectionCount = sessionId =>
    makeRequest(sessionId, "GBSpectrumService.GetNumProjections", {}, {});

export const calculateProjection = (sessionId, projectionId) =>
    makeRequest(sessionId, "GBSpectrumService.CalculateProjection", {}, {
        Proj: projectionId
    });

export const getIndicators = (sessionId, moduleId) =>
    makeRequest(sessionId, "GBSpectrumService.GetResultDisplays", {}, {
        ModID: moduleId
    });

export const getResultTable = (sessionId, projectionId, moduleId, indicatorId, firstYear) =>
    makeRequest(sessionId, "GBSpectrumService.GetResultTable", {}, {
        Proj: projectionId,
        ModID: moduleId,
        IndID: indicatorId,
        ChartOptions: {
            FirstYear: firstYear
        }
    });

export const getResultChart = (sessionId, projectionId, moduleId, indicatorId, firstYear) =>
    makeRequest(sessionId, "GBSpectrumService.GetResultChart", {}, {
        Proj: projectionId,
        ModID: moduleId,
        IndID: indicatorId,
        ChartOptions: {
            FirstYear: firstYear
        }
    });
